﻿using System;

namespace week03_exercise05
{
	class MainClass
	{
		public static void Main(string[] args)
		{
			var colours = new string[5] { "red", "blue", "orange", "white", "black" };
			Array.Sort(colours);
			Array.Reverse(colours);
			Console.WriteLine(string.Join(",", colours));

		}
	}
}
